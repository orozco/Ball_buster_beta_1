import sys, pygame, random
from pygame.locals import *

pygame.init()

def cargar_imagen(filename, transparent = False):
    try: image = pygame.image.load(filename)
    except pygame.error, message:
        raise SystemExit, message
    image = image.convert()
    if transparent:
        color = image.get_at((0,0))
        image.set_colorkey(color, RLEACCEL)
    return image





def main():
    pantalla = pygame.display.set_mode((820,620),0,32)
    imagen = pygame.image.load("image/ladron.png")
    pygame.display.set_caption("Ball Buster BETA1")
    fondo = cargar_imagen('image/fondo.png')
    x = 10
    y = 10

    reloj = pygame.time.Clock()
    while True:
        for eventos in pygame.event.get():
            if eventos.type == QUIT:
                sys.exit(0)
        for eventos in pygame.event.get():
            if eventos.type == pygame.QUIT:
                exit()
        pulsada = pygame.key.get_pressed()

        if pulsada[K_w]:
            y -= 5
        if pulsada[K_s]:
            y += 5
        if pulsada[K_a]:
            x -= 5
        if pulsada[K_d]:
            x += 5
        reloj.tick(25)
        pantalla.blit(fondo, (0,0))
        pantalla.blit(imagen,(x,y))
        pygame.display.update()


if __name__ == '__main__':
    pygame.init()
    main()
    
