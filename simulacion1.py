#! /usr/bin/env python2
# -*- coding: utf-8 -*-

#--------------------------------------------------------------------#
# Programa: Escenario para simular robos..
# Autores: Efrain "The Ball Buster"				     #
#								     #
# Herramientas De Trabajo: Python, Pygame, Vim, Gimp
# Licencia: GNU/GPL
#--------------------------------------------------------------------#

# Modulos
#--------------------------------------------------------------------#

import sys, pygame, random
import numpy as np
from pygame.locals import *

#--------------------------------------------------------------------#

# Constantes
#--------------------------------------------------------------------#

WIDTH = 820
HEIGHT = 620
CONTER = 0

#--------------------------------------------------------------------#

# Variables Globales
#--------------------------------------------------------------------#

vectorRutas = []
vectorFerras = []
#--------------------------------------------------------------------#

# Clases
#--------------------------------------------------------------------#

class creacionObjeto(pygame.sprite.Sprite):
    pos_ini = ()
    pos_fin = ()

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = cargar_imagen("image/ladron.png", True)
        self.posicion = (1,1)
        self.rect = pygame.Rect(self.posicion[0], self.posicion[1], 2, 2)
        self.contRutas = 0
        self.contPuntos = 0
        self.rnd = 0
        self.estado = True
        self.rata = True
        self.banRnd = True
        self.indVector  = 0
        self.vectorPuntos = []

    def actualizar(self):
        pygame.sprite.Sprite.update(self)

        global vectorRutas

        if self.banRnd:
            self.rnd = random.randrange(len(vectorRutas[self.contRutas].nodo_x))
            self.pos_ini = vectorRutas[self.contRutas].posicion
            self.pos_fin = (vectorRutas[self.contRutas].nodo_x[self.rnd], vectorRutas[self.contRutas].nodo_y[self.rnd])
            self.indVector = self.verificarRuta(self.pos_fin, vectorRutas)
            self.vectorPuntos = interpolacion(self.pos_ini, self.pos_fin, 25)

        self.banRnd = False
        self.posicion = (self.vectorPuntos[self.contPuntos].posicion[0], self.vectorPuntos[self.contPuntos].posicion[1])
        self.rect.x = self.posicion[0]
        self.rect.y = self.posicion[1]
        rectTemp = pygame.Rect(self.pos_fin[0], self.pos_fin[1], 2, 2)

        if self.rect.colliderect(rectTemp):
            self.contPuntos = 0
            self.contRutas = self.indVector
            self.banRnd = True

    def Caturar(self):
        global CONTER
        for o in vectorFerras:
            if o != self and o.rata == True and self.rata == False:
                rectTemp2 = pygame.Rect(o.posicion[0], o.posicion[1], 2, 2)
                if rectTemp2.colliderect(self.rect):
                    o.posicion = (468.01,28)
                    o.contRutas = 10 - 1
                    o.banRnd = True
                    o.contPuntos = 0
                    CONTER += 1
                    print (CONTER)



    def verificarRuta(self,p, vec):
        ind = 0
        for i in vec:
            if i.posicion == p:
                break
            ind += 1
        return ind




class Rutas():
    def __init__(self, pos):
        self.posicion = pos
        self.nodo_x = []
        self.nodo_y = []
        self.indice = 0

class Punto():
    posicion = ()


#--------------------------------------------------------------------#

# Funciones
#--------------------------------------------------------------------#

def cargar_imagen(filename, transparent = False):
    try: image = pygame.image.load(filename)
    except pygame.error, message:
        raise SystemExit, message
    image = image.convert()
    if transparent:
        color = image.get_at((0,0))
        image.set_colorkey(color, RLEACCEL)
    return image

def interpolacion(pos_ini, pos_fin, iteraciones):
    m = (np.float32(pos_fin[1]) - np.float32(pos_ini[1])) / (np.float32(pos_fin[0]) - np.float32(pos_ini[0]))
    a =  (-m * pos_ini[0]) + pos_ini[1]
    numeroln = (np.float32(pos_fin[0]) - np.float32(pos_ini[0])) / np.float32(iteraciones)

    newy = 0
    newx = pos_ini[0]
    limitador = 0
    vecTemp = []

    while (limitador < iteraciones):
        punto = Punto()

        newx = newx + numeroln
        newy = (m * newx) + a

        punto.posicion = (newx, newy)
        vecTemp.append(punto);
        limitador += 1
    return vecTemp

def cargarDatos():
    global vectorRutas

# Zona de Arriba
#------------------------------------------------------------------#
    r = Rutas((10, 10))
    r.nodo_x.append(10.01)
    r.nodo_y.append(240)
    r.nodo_x.append(156)
    r.nodo_y.append(10)
    r.indice = 1
    vectorRutas.append(r)

    r = Rutas((156, 10))
    r.nodo_x.append(10)
    r.nodo_y.append(10)
    r.nodo_x.append(312)
    r.nodo_y.append(10)
    r.nodo_x.append(156.01)
    r.nodo_y.append(240)
    r.indice = 2
    vectorRutas.append(r)

    r = Rutas((312, 10))
    r.nodo_x.append(156)
    r.nodo_y.append(10)
    r.nodo_x.append(468)
    r.nodo_y.append(10)
    r.nodo_x.append(312.01)
    r.nodo_y.append(240)
    r.indice = 3
    vectorRutas.append(r)

    r = Rutas((468, 10))
    r.nodo_x.append(312)
    r.nodo_y.append(10)
    r.nodo_x.append(624)
    r.nodo_y.append(10)
    r.nodo_x.append(468.01)
    r.nodo_y.append(240)
    r.indice = 4
    vectorRutas.append(r)

    r = Rutas((624, 10))
    r.nodo_x.append(468)
    r.nodo_y.append(10)
    r.nodo_x.append(780)
    r.nodo_y.append(10)
    r.nodo_x.append(624.01)
    r.nodo_y.append(240)
    r.indice = 5
    vectorRutas.append(r)

    r = Rutas((780,10))
    r.nodo_x.append(624)
    r.nodo_y.append(10)
    r.nodo_x.append(780.01)
    r.nodo_y.append(240)
    r.indice = 6
    vectorRutas.append(r)
#------------------------------------------------------------------#

#------------------------------------------------------------------#

#------------------------------------------------------------------#
# Zona del Medio
#------------------------------------------------------------------#
    r = Rutas((10.01, 240))
    r.nodo_x.append(10)
    r.nodo_y.append(10)
    r.nodo_x.append(156.01)
    r.nodo_y.append(240)
    r.nodo_x.append(10)
    r.nodo_y.append(580)
    r.indice = 7
    vectorRutas.append(r)

    r = Rutas((156.01, 240))
    r.nodo_x.append(10.01)
    r.nodo_y.append(240)
    r.nodo_x.append(156)
    r.nodo_y.append(10)
    r.nodo_x.append(312.01)
    r.nodo_y.append(240)
    r.nodo_x.append(156)
    r.nodo_y.append(580)
    r.indice = 8
    vectorRutas.append(r)


    r = Rutas((312.01, 240))
    r.nodo_x.append(156.01)
    r.nodo_y.append(240)
    r.nodo_x.append(312)
    r.nodo_y.append(10)
    r.nodo_x.append(468.01)
    r.nodo_y.append(240)
    r.nodo_x.append(312)
    r.nodo_y.append(580)
    r.indice = 9
    vectorRutas.append(r)

    r = Rutas((468.01, 240))
    r.nodo_x.append(312.01)
    r.nodo_y.append(240)
    r.nodo_x.append(468)
    r.nodo_y.append(10)
    r.nodo_x.append(624.01)
    r.nodo_y.append(240)
    r.nodo_x.append(468)
    r.nodo_y.append(580)
    r.indice = 10
    vectorRutas.append(r)

    r = Rutas((624.01, 240))
    r.nodo_x.append(468.01)
    r.nodo_y.append(240)
    r.nodo_x.append(624)
    r.nodo_y.append(10)
    r.nodo_x.append(780.01)
    r.nodo_y.append(240)
    r.nodo_x.append(624)
    r.nodo_y.append(580)
    r.indice = 11
    vectorRutas.append(r)


    r = Rutas((780.01, 240))
    r.nodo_x.append(624.01)
    r.nodo_y.append(240)
    r.nodo_x.append(780)
    r.nodo_y.append(10)
    r.nodo_x.append(780)
    r.nodo_y.append(580)
    r.indice = 12
    vectorRutas.append(r)
#------------------------------------------------------------------#

# Zona De Abajo
#------------------------------------------------------------------#
    r = Rutas((10,580))
    r.nodo_x.append(10.01)
    r.nodo_y.append(240)
    r.nodo_x.append(156)
    r.nodo_y.append(580)
    r.indice = 13
    vectorRutas.append(r)

    r = Rutas((156,580))
    r.nodo_x.append(10)
    r.nodo_y.append(580)
    r.nodo_x.append(156.01)
    r.nodo_y.append(240)
    r.nodo_x.append(312)
    r.nodo_y.append(580)
    r.indice = 14
    vectorRutas.append(r)

    r = Rutas((312,580))
    r.nodo_x.append(156)
    r.nodo_y.append(580)
    r.nodo_x.append(312.01)
    r.nodo_y.append(240)
    r.nodo_x.append(468)
    r.nodo_y.append(580)
    r.indice = 15
    vectorRutas.append(r)

    r = Rutas((468,580))
    r.nodo_x.append(312)
    r.nodo_y.append(580)
    r.nodo_x.append(468.01)
    r.nodo_y.append(240)
    r.nodo_x.append(624)
    r.nodo_y.append(580)
    r.indice = 16
    vectorRutas.append(r)

    r = Rutas((624,580))
    r.nodo_x.append(468)
    r.nodo_y.append(580)
    r.nodo_x.append(624.01)
    r.nodo_y.append(240)
    r.nodo_x.append(780)
    r.nodo_y.append(580)
    r.indice = 17
    vectorRutas.append(r)

    r = Rutas((780,580))
    r.nodo_x.append(624)
    r.nodo_y.append(580)
    r.nodo_x.append(780.01)
    r.nodo_y.append(240)
    r.indice = 18
    vectorRutas.append(r)

#----------------------------------------------------------------- #

# Policias
#------------------------------------------------------------------#
    """pol = creacionObjeto()
    pol.posicion = (780,580)
    pol.contRutas = 18 - 1
    pol.image = cargar_imagen("image/poli.png", True)
    pol.rata = False
    vectorFerras.append(pol)

    pol = creacionObjeto()
    pol.posicion = (780,10)
    pol.contRutas = 6 - 1
    pol.rata = False
    pol.image = cargar_imagen("image/poli.png", True)
    vectorFerras.append(pol)

    pol = creacionObjeto()
    pol.posicion = (10,10)
    pol.contRutas = 1 - 1
    pol.rata = False
    pol.image = cargar_imagen("image/poli.png", True)
    vectorFerras.append(pol)

    rat = creacionObjeto()
    rat.posicion = (10,580)
    rat.contRutas = 13 - 1
    rat.rata = False
    rat.image = cargar_imagen("image/poli.png", True)
    vectorFerras.append(pol) """
#------------------------------------------------------------------#
# Ratero
#------------------------------------------------------------------#
    rat = creacionObjeto()
    rat.posicion = (468.01,240)
    rat.contRutas = 10 - 1
    vectorFerras.append(rat)




# Ejecucion
#------------------------------------------------------------------#

def main():
    screen = pygame.display.set_mode((WIDTH,HEIGHT))
    pygame.display.set_caption("Ball Buster BETA1")

    fondo = cargar_imagen('image/fondo.png')

    global vectorFerras
    cargarDatos()

    while True:
        for eventos in pygame.event.get():
            if eventos.type == QUIT:
                sys.exit(0)
        screen.blit(fondo, (0,0))
        for rats in vectorFerras:
            if rats.estado == True:
                rats.actualizar()
                rats.contPuntos += 1
                rats.Caturar()
                screen.blit(rats.image, rats.posicion)
        pygame.display.flip()
        pygame.time.delay(30)
    return 0

if __name__ == '__main__':
    pygame.init()
    main()


#--------------------------------------------------------------------#
